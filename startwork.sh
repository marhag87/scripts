#!/bin/bash

if [[ $1 == '' ]]; then
  exit 1
fi

SAVED_IFS=$IFS
IFS='/'
read -r -a directories <<< "$1"
IFS=$SAVED_IFS

envs=$(find ~/.virtualenvs/ -maxdepth 1 -mindepth 1 -type d -printf '%f\n')

for dir in ${directories[@]}; do
  for env in ${envs[@]}; do
    if [[ "$env" == "$dir" ]]; then
      workenv=$env
    fi
  done
done

python=""
if [[ -s "/opt/rh/rh-python35/enable" ]]; then
  python="source /opt/rh/rh-python35/enable &&"
fi

tmux new-window -c $1
tmux split-window -h -c $1
tmux split-window -v -p 40 -c $1
tmux set-window-option synchronize-panes
tmux send-keys "$python workon $workenv" Enter
tmux set-window-option synchronize-panes off
tmux select-pane -L
